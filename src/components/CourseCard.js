// Bootstrap
import { Row, Col, Card, Button} from 'react-bootstrap';

export default function CourseCard () {
	return (
			<Row>
				<Col>
					<Card>
						<Card.Body>
							<Card.Title><strong>Introduction to React-Bootstrap</strong></Card.Title>
							<Card.Text>
							<strong>Description:</strong>
							<p>Integrating Bootstrap with React allows web developers to write much cleaner code, 
							thus reducing the time spent on the frontend</p>
							<strong>Price:</strong>
							<p>P4,999</p>
							</Card.Text>
							<Button variant='primary'>Enroll</Button>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		)
}