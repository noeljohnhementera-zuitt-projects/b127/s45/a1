
/*
Separation of concerns
	* Put this in a separate folder. AppNavbar.js is a component
*/

/*function AppNavBar(){
	return(
		)
}
export default AppNavBar;	// or
*/
// Kinukuha yung NavBar sa package
//import NavBar from 'react-bootstrap/NavBar';
//import Nav from 'react-bootstrap/Nav';

import {Navbar, Nav} from 'react-bootstrap';
// We can do it this way unlike above

/*
// Old way - Using Class Based
import { Component } from 'react';

export default class AppNavBar extends Component(){
	return(
			<Navbar bg="light" expand="lg"> 
				<Navbar.Brand href="#home">Zuitt Booking System</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" /> 
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto"> 
						<Nav.Link href="#home">Home</Nav.Link>
						<Nav.Link href="#courses">Courses</Nav.Link>
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		)
}*/


// We use Pascal, capital letter first letter 
	// = para hindi mag-overlap sa HTML tags na ginagamit
	// para makuha ang React syntax
// <!-- Kailngan may forward slash para hindi mag-error -->
// <!-- className = class but it is used in React -->
// import to index.js
// className is similar to class sa HTML / CSS
// <Navbar.Toggle> and <Navbar.Collapse> is used to have a hamburger icon

// Function based / components easy to create a task sa function unlike the class based
export default function AppNavBar(){
	return(
			<Navbar bg="light" expand="lg"> 
				<Navbar.Brand href="#home">Zuitt Booking System</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" /> 
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto"> 
						<Nav.Link href="#home">Home</Nav.Link>
						<Nav.Link href="#courses">Courses</Nav.Link>
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		)
}
