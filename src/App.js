//import React from 'react';
// Alternative
import { Fragment } from 'react';
// <React.fragment> to enclose components into a parent element para ma-avoid ang error
import './App.css';

// Dito magrender ng multiple components
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';

// Dinala na sa home.js
/*import Banner from './components/Banner';
import Highlights from './components/Highlights';*/

// Bootstrap
import { Container } from 'react-bootstrap'; 

function App() {          // Always dapat na may function
  return (                // It usually returns jsx
    <Fragment>
      <AppNavbar />
      <Container>
        <Home/>
      </Container>
    </Fragment>
  );
}

export default App;       // To export the file in the function


/*

<div className="App">
      <h1>Hello World</h1>
</div>

Notes:

With the React fragment, we can group multiple components and avoid adding extra code

<Fragment> is preferred over <></> (shortcut) because it is not universal and can cause
problems in some other editors

JSX Syntax
JSX or JavaScript XML is an extension to the syntax of JS. It allows us to write 
HTML-like syntax written within our React js projects and it includes JS features as well.

  jsx - extension na pwede basahin ang JavaScript syntaxes
  Need to install JS(Babel) to run jsx for code readability
    1 Ctrl + Shift + P
    2. In the input field, type the wprd "install" and select the "package control: install package"
       option to trigger an installation of an add-on feature
    3. Type "babel" in the input field to be installed
*/
